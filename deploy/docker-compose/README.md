# Docker-compose

Docker-compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.

## 1. Description of containers configuration

In the `debian-cloud-image-finder` we have 3 containers as seen in table 01. In addition to the explanation of the flags within the containers, we have the explanation of the env files in the [item 2](#21-debian-cloud-image-finder).

| Name    |
| ------- |
| app     |
| db      |
| traefik |

> Table 01 - Name of the containers.

### 1.1 App

| Flag | Command | Description |
| -------------- | -------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------  |
| image          | registry.salsa.debian.org/cloud-team/image-finder:latest | Image that defines the container.                                                                                                                                          |
| container_name | debian-cloud-image-finder                                | Name given to the container.                                                                                                                                               |
| restart        | unless-stopped                                           | Similar to always, except that when the container is stopped (manually or otherwise), it is not restarted even after Docker daemon restarts.                               |
| labels         | [#app.env](#21-debian-cloud-image-finder)                | The `LABEL` instruction adds metadata to an image.                                                                                                                         |
| ports          | 5000:5000                                                | Local host port 5000, container port 5000.                                                                                                                                 |
| env_file       | cloud-image-finder.env                                   | Instruction that creates and assigns a value to a variable within the image. [See more](#21-debian-cloud-image-finder)                                                     |
| depends_on     | db: condition: service_healthy                           | A healthcheck indicates that you want a dependency to wait for **container db** to be `healthy` (as indicated by a successful state from the healthcheck) before starting. |
| links          | db                                                       | Links allow you to define extra aliases by which a service is reachable from another service.                                                                              |

> Table 02 - Description of `app` container configuration.

### 1.2 Database (db)


| Flag           | Command                                        | Description                                                                                                                                  |
| -------------- | ---------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| container_name | debian-cloud-image-finder-postgres             | Name given to the container.                                                                                                                 |
| image          | postgres:11.3                                  | Image that defines the container.                                                                                                            |
| restart        | unless-stopped                                 | Similar to always, except that when the container is stopped (manually or otherwise), it is not restarted even after Docker daemon restarts. |
| env_file       | postgres.env                                   | Instruction that creates and assigns a value to a variable within the image. [See more](#22-postgres)                                        |
| ports          | 5432:5432                                      | Local host port 5432, container port 5432.                                                                                                   |
| healthcheck    | [db.healthcheck](#42-container-db-healthcheck) | check de container health by running te command insite the container.                                                                        |
| volumes        | `./data/postgresql:/var/lib/postgresql/data`   | Connects host path `./data/postgresql`  with container path `/var/lib/postgresql/data`.                                                      |

> Table 03 - Description of `db` container configuration.


### 1.3 Traefik

> This service is optional and is used as a proxy, handling, attaching and renewing SSL certificates.


| Flag           | Command                                                                 | Description                                                                                                                                  |
| -------------- | ----------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| container name | traefik                                                                 | Name given to the container.                                                                                                                 |
| image          | `traefik:v2.0`                                                          | Image that defines the container.                                                                                                            |
| restart        | unless-stopped                                                          | Similar to always, except that when the container is stopped (manually or otherwise), it is not restarted even after Docker daemon restarts. |
| command        | [traefik.command](#43-container-traefik-command)                        | Assigning values ​​to flag.                                                                                                                    |
| labels         | [traefik.labels](#44-container-traefik-labels)                          | The `LABEL` instruction adds metadata to an image.                                                                                           |
| ports          | `80:80` `443:443`                                                       | Local host port 80 and 433, container port 80 e 433.                                                                                         |
| volumes        | `/var/run/docker.sock:/var/run/docker.sock:ro` `./acme.json:/acme.json` | Connects host `/var/run/docker.sock`  with  container `/var/run/docker.sock:ro` and `./acme.json` with `/acme.json`.                         |

> Table 04 - Description of `traefik` container configuration.


## 2. Description of env files configuration


### 2.1 debian-cloud-image-finder

| Key                              | Values                                                 | Description                                                                                                       |
| -------------------------------- | ------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------- |
| FLASK_ENV                        | production                                             | Control the environment, can be `production`, `development` and `testing`.                                        |
| FLASK_APP                        | debian_image_finder:create_app                         | Variable is used to specify how to load the application.                                                          |
| DYNACONF_HOST                    | `image-finder.debian.net`                              | Sets the app hostname.                                                                                            |
| DYNACONF_SECRET_KEY              | ''                                                     | Sets secret key, a `16 characters` hexadecimal, can be generated with `uuid.uuid4().hex`.                         |
| DYNACONF_GITLAB_CLIENT_ID        | ''                                                     | Sets gitlab client ID.                                                                                            |
| DYNACONF_GITLAB_CLIENT_SECRET.   | ''                                                     | Sets gitlab client secret.                                                                                        |
| DYNACONF_VERIFY_TLS_CERTIFICATES | false                                                  | Whether or not verify TLS certificates when authenticating with Salsa SSO. Set to false when using `development`. |
| DYNACONF_SQLALCHEMY_DATABASE_URI | `postgresql://dcif:<db_password>@db:5432/image-finder` | Sets the database.                                                                                                |

> Table 05 - Description of `cloud-image-finder.env` configuration.

### 2.2 Postgres

| Key               | Values            | Description                 |
| ----------------- | ----------------- | --------------------------- |
| POSTGRES_PASSWORD | ''                | Sets the database password. |
| POSTGRES_DB       | image-finder      | Sets the database name.     |
| POSTGRES_USER     | dcif              | Sets the database user.     |
| POSTGRES_HOST     | db                | Sets the database host.     |
| POSTGRES_PORT     | 5432              | Sets the database port.     |


> Table 06 -  Description of `postgres.env` configuration.

## 3 How to run

- Start containers: `make run`
- Stop containers: `make stop`

---

**If you have a preference for using docker:**

- Up container: `docker-compose up`
- Down container: `docker-compose down`
---
- Get inside the container bash debian-cloud-image-finder: `docker exec -it debian-cloud-image-finder bash`
- Get inside the container bash postgres: `docker exec -it debian-cloud-image-finder-postgres bash`
- Get inside the container bash traefik: `docker exec -it traefik bash`

## 4 Appendix

### 4.1 Cointainer app labels

| Labels                                                           |
| ---------------------------------------------------------------- |
| `traefik.http.routers.app.rule=Host(\"image-finder.debian.net")` |
| `traefik.http.routers.app.entrypoints=websecure`                 |
| `traefik.http.routers.app.tls=true`                              |
| `traefik.http.routers.app.tls.certresolver=letsencrypt`          |

> Table 07 - Labels from container `app`.

<a id="db.healthcheck"></a>

### 4.2 Container db healthcheck

| healthcheck                               |
| ----------------------------------------- |
| test: ["CMD-SHELL", "pg_isready -U dcif"] |
| interval: 10s                             |
| timeout: 5s                               |
| retries: 5                                |

> Table 08 - Healthcheck from container `db`.

### 4.3 Container traefik command

| command                                                                                          |
| ------------------------------------------------------------------------------------------------ |
| --entrypoints.web.address=:80                                                                    |
| --entrypoints.websecure.address=:443                                                             |
| --providers.docker                                                                               |
| --api                                                                                            |
| --certificatesresolvers.letsencrypt.acme.caserver=https://acme-v02.api.letsencrypt.org/directory |
| --certificatesresolvers.letsencrypt.acme.email=user\@email.com                                   |
| --certificatesresolvers.letsencrypt.acme.storage=/acme.json                                      |
| --certificatesresolvers.letsencrypt.acme.tlschallenge=true                                       |

> Table 09 - Command from container `traefik`.

### 4.4 Container traefik labels


| labels                                                                   |
| ------------------------------------------------------------------------ |
| "traefik.http.routers.http-catchall.rule=hostregexp(\`{host:.+}`)"       |
| "traefik.http.routers.http-catchall.entrypoints=web"                     |
| "traefik.http.routers.http-catchall.middlewares=redirect-to-https"       |
| "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https" |

> Table 10 -  Labels from container `traefik`.