# This function should receive the module name that will be tested as a "name-module"
# parameter.
# e.g.
# To test "api" module we should run: make test-module module-name=api
# If is not specified a correct module name, a warning message will be shown, describing how to use the command.
# If is a correct module name is specified, the tests will be executed and will print success message.

if [ -z "$1" ]
  then
    echo "You should pass a valid test module. e.g. To test 'api' module you should run the command: make test-module module-name=api"
	exit 1
fi

docker run -it --rm \
	--env-file testing.env \
	--entrypoint="" \
	-v $PWD:/debian-image-finder \
	debian-image-finder \
	nose2-3.8 debian_image_finder.tests.$1