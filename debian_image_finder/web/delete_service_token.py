from flask import redirect, url_for, flash
from flask_login import login_required, current_user
from debian_image_finder.models.service_token import ServiceToken
from debian_image_finder.utils.admin import admin_required


@login_required
@admin_required
def delete_service_token(token_public_id):

    service_token = ServiceToken.find_by_public_id(token_public_id)
    if not service_token:
        flash(
            'Something went wrong, could not delete token.',
            category='error'
        )
        return redirect(url_for('.service_tokens'))

    if service_token.user_id == current_user.id:
        service_token.delete_from_db()
        flash('Successfully deleted token.', category='success')
        return redirect(url_for('.service_tokens'))

    return redirect(url_for('.service_tokens'))
