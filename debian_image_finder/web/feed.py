from flask import render_template, make_response, request
from debian_image_finder.models.image import Image
from debian_image_finder.models.provider import Provider


def feed(vendor):
    page = request.args.get('page', default=1, type=int)
    provider = Provider.find_by_vendor(vendor)
    all_images_from_provider = Image.find_by_provider(provider).paginate(
        page=page, per_page=10
    )
    template = render_template(
        'feed.xml',
        provider=provider,
        images=all_images_from_provider,
        baseurl=request.url_root
    )

    response = make_response(template)
    response.headers['Content-Type'] = 'application/xml'
    return response
