from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField


class ProviderSearchForm(FlaskForm):
    ref = StringField('Ref')
    version = StringField('Version')
    release = SelectField('Release', choices=[('', 'Release')], default=1)
    img_type = SelectField('Type', choices=[('release', 'Type')], default=1)
    arch = SelectField('Arch', choices=[('', 'Arch')], default=1)
    region = SelectField('Region', choices=[('', 'Region')], default=1)
    submit = SubmitField('Search')
