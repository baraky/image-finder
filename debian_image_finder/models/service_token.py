import datetime
import uuid
from typing import List
from debian_image_finder.extensions.database import db


class ServiceToken(db.Model):
    __tablename__ = 'service_tokens'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    public_id = db.Column(db.String(255), unique=True)
    name = db.Column(db.String(80), nullable=False)
    created_at = db.Column(db.DateTime(), nullable=False)
    expires_at = db.Column(db.DateTime(), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    user = db.relationship('User', foreign_keys=user_id)

    def __init__(self, name, user_id, expires_at):
        self.public_id = str(uuid.uuid4())
        self.name = name
        self.expires_at = expires_at
        self.created_at = datetime.datetime.utcnow()
        self.user_id = user_id

    @classmethod
    def find_by_id(cls, _id) -> 'ServiceToken':
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_public_id(cls, public_id) -> 'ServiceToken':
        return cls.query.filter_by(public_id=public_id).first()

    @classmethod
    def find_by_name(cls, name) -> 'ServiceToken':
        return cls.query.filter_by(name=name).first_or_404()

    @classmethod
    def find_by_body(cls, vendor) -> 'ServiceToken':
        return cls.query.filter_by(vendor=vendor).first_or_404()

    @classmethod
    def find_all_by_user_id(cls, user_id) -> List['ServiceToken']:
        return cls.query.filter_by(user_id=user_id).all()

    @classmethod
    def find_all(cls) -> List['ServiceToken']:
        return cls.query.all()

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return 'ServiceToken(name=%s, created_at=%s)' % (
            self.name, self.created_at)
