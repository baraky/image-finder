from flasgger import swag_from
from flask import make_response, request, jsonify
from flask_restful import Resource
from wtforms.validators import ValidationError
from debian_image_finder.models.image import Image
from debian_image_finder.schemas.image import image_schema
from debian_image_finder.utils.token import valid_token_required


class ImageAPI(Resource):

    @swag_from('docs/image/get.yml')
    def get(self, uid):
        image = Image.find_by_uid(uid)
        return image_schema.dump(image), 200

    @valid_token_required
    @swag_from('docs/image/post.yml')
    def post(self, user):
        data = request.get_json()
        try:
            image = image_schema.load(data)
        except ValidationError as error:
            return make_response(jsonify(error), 401)

        image.save_to_db()

        return image_schema.dump(image), 201

    @valid_token_required
    @swag_from('docs/image/put.yml')
    def put(self, user, uid):
        image = Image.find_by_uid(uid)
        data = request.get_json()
        image.update(data)
        image.save_to_db()

        return image_schema.dump(image), 200

    @valid_token_required
    @swag_from('docs/image/delete.yml')
    def delete(self, user, uid):
        image = Image.find_by_uid(uid)
        image.delete_from_db()

        return {'message': "Image deleted successfully."}, 200
