from flasgger import swag_from
from flask_restful import Resource
from marshmallow import ValidationError
from flask import request
from debian_image_finder.models.provider import Provider
from debian_image_finder.schemas.provider import provider_schema
from debian_image_finder.api.user import valid_token_required


class ProviderAPI(Resource):

    @swag_from('docs/provider/get.yml')
    def get(self, vendor=None):
        provider = Provider.find_by_vendor(vendor)
        if not provider:
            return {'error': "Provider not found."}, 404
        return provider_schema.dump(provider), 200

    @valid_token_required
    @swag_from('docs/provider/post.yml')
    def post(self, user):
        data = request.get_json()
        try:
            provider = provider_schema.load(data)
        except ValidationError as error:
            return {'error': error.messages['_schema']}, 409

        provider.save_to_db()

        return provider_schema.dump(provider), 201

    @valid_token_required
    @swag_from('docs/provider/put.yml')
    def put(self, user, vendor):
        provider = Provider.find_by_vendor(vendor)
        if not provider:
            return {'error': "Provider not found."}, 404
        data = request.get_json()
        provider.update(data)
        provider.save_to_db()

        return provider_schema.dump(provider), 200

    @valid_token_required
    @swag_from('docs/provider/delete.yml')
    def delete(self, user, vendor):
        provider = Provider.find_by_vendor(vendor)
        if not provider:
            return {'error': "Provider not found."}, 404

        provider.delete_from_db()

        return {'message': "Provider deleted successfully."}, 200
