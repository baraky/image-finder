from flask_restful import Resource
from flasgger import swag_from
from debian_image_finder.models.user import User
from debian_image_finder.schemas.user import user_schema
from debian_image_finder.utils.token import valid_token_required


class UserAPI(Resource):

    @valid_token_required
    @swag_from('docs/user/get.yml')
    def get(self, user, public_id=None):
        user = User.find_by_public_id(public_id)
        if not user:
            return {'error': 'User not found.'}, 401
        return user_schema.dump(user), 200

    @valid_token_required
    @swag_from('docs/user/delete.yml')
    def delete(self, user, public_id=None):
        user = User.find_by_public_id(public_id)
        if not user:
            return {'error': 'User not found.'}, 401
        user.delete_from_db()

        return {'message': 'The user has been deleted.'}, 200
