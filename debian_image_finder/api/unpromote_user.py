from flasgger import swag_from
from flask_restful import Resource
from debian_image_finder.models.user import User
from debian_image_finder.utils.token import valid_token_required


class UnpromoteUserAPI(Resource):

    @valid_token_required
    @swag_from('docs/unpromote_user/put.yml')
    def put(self, user, public_id):
        user = User.find_by_public_id(public_id)

        if not user:
            return {'error': 'User not found.'}, 404

        user.admin = False
        user.save_to_db()

        return {'message': 'The user has been unpromoted.'}, 200
