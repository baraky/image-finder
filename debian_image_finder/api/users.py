from flasgger import swag_from
from flask_restful import Resource
from debian_image_finder.models.user import User
from debian_image_finder.schemas.user import users_schema
from debian_image_finder.utils.token import valid_token_required


class UsersAPI(Resource):

    @valid_token_required
    @swag_from('docs/users/get.yml')
    def get(self, user, public_id=None):
        users = User.find_all()
        return users_schema.dump(users), 200
