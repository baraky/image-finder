import logging
from flasgger import swag_from
from flask import request
from flask_restful import Resource
from debian_image_finder.schemas.image import images_schema
from debian_image_finder.utils.token import valid_token_required
from debian_image_finder.utils.publish import publish_images


class PublishAPI(Resource):

    @valid_token_required
    @swag_from('docs/publish/post.yml')
    def post(self, user=None):
        try:
            images = publish_images(data=request.json)
            result = images_schema.dump(images)
            return {'images': result}, 201
        except Exception as exception:
            logging.error(exception)
            return {}, 400
