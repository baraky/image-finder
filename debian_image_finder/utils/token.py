import jwt
from flask import make_response, request, jsonify
from functools import wraps
from datetime import datetime, timedelta
from dynaconf import settings
from debian_image_finder.models.user import User
from debian_image_finder.models.service_token import ServiceToken


def valid_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'Authorization' not in request.headers:
            return make_response(
                jsonify({
                    'error': 'Authorization header not in request.'
                }), 401
            )

        jwt_value = request.headers['Authorization']
        jwt_value = jwt_value.replace('Bearer', '').replace(' ', '')

        is_valid, response = verify_token(jwt_value)
        if not is_valid:
            return response

        user = check_public_id(payload)
        if not user:
            return make_response(jsonify({
                'error': 'User not allowed to perform this action.'
            }), 401)


        return f(user, *args, **kwargs)

    return decorated


def verify_token(token):
    if not token:
        return False, make_response(
            jsonify({'error': 'Token is missing.'}), 401
        )
    try:
        payload = decode_jwt(token)

    except jwt.ExpiredSignatureError:
        return False, make_response(
            jsonify({'error': 'Signature has expired.'}), 401
        )
    except jwt.DecodeError:
        return False, make_response(
            jsonify({'error': 'Error decoding signature.'}), 401
        )
    except jwt.InvalidTokenError:
        return False, make_response(
            jsonify({'error': 'Invalid token.'}), 401
        )

    if not ServiceToken.find_by_public_id(
        payload['extra_data']['token_public_id']
    ):
        return False, make_response(
            jsonify({'error': 'Token not found.'}), 404
        )

    return True, None

def check_public_id(payload):
    user = User.find_by_public_id(payload['extra_data']['user_public_id'])
    if not user:
        return None

    if not user.admin:
        return None

    return user


def generate_payload(issuer, minutes=30, days=0, **extra_data):
    """
    :param issuer: identifies the principal that issued the token.
    :type issuer: str
    :param minutes: number of minutes that the token will be valid.
    :type minutes: int
    :param hours: number of hours that the token will be valid.
    :type hours: int
    :param days: number of days that the token will be valid.
    :type days: int
    :param extra_data: extra data to be added to the payload.
    :type extra_data: dict
    :rtype: dict
    """

    now = datetime.utcnow()
    issued_at = now
    expiration = now + timedelta(minutes=minutes, days=days)
    payload = {
        'iss': issuer,
        'exp': expiration,
        'iat': issued_at,
    }

    if extra_data:
        payload.update(**extra_data)

    return payload


def encode_jwt(payload, headers=None):
    """
    :type payload: dict
    :type headers: dict, None
    :rtype: str
    """

    algorithm = settings.JWT_ENC_ALGORITHM

    jwt_key = settings.SECRET_KEY
    if not jwt_key:
        raise Exception('Missing setting SECRET_KEY')

    encoded = jwt.encode(
        payload,
        jwt_key,
        algorithm=algorithm,
        headers=headers
    )
    return encoded


def decode_jwt(jwt_value):
    """
    :type jwt_value: str
    """

    try:
        headers_enc, payload_enc, verify_signature = jwt_value.split(".")
    except ValueError:
        raise jwt.InvalidTokenError()

    algorithm = settings.JWT_ENC_ALGORITHM
    jwt_key = settings.SECRET_KEY
    if not jwt_key:
        raise Exception('Missing setting SECRET_KEY')

    decoded = jwt.decode(jwt_value, jwt_key, algorithms=[algorithm])

    return decoded
