from flask_dance.contrib.gitlab import make_gitlab_blueprint
from flask_dance.consumer.storage.sqla import SQLAlchemyStorage
from debian_image_finder.extensions.database import db
from debian_image_finder.models.oauth import OAuth
from flask_login import current_user
from dynaconf import settings

blueprint = make_gitlab_blueprint(
    client_id=settings.GITLAB_CLIENT_ID,
    client_secret=settings.GITLAB_CLIENT_SECRET,
    hostname=settings.GITLAB_HOSTNAME,
    verify_tls_certificates=settings.VERIFY_TLS_CERTIFICATES
)


def init_app(app):
    app.register_blueprint(blueprint, url_prefix="/gitlab_login")
    blueprint.storage = SQLAlchemyStorage(
        OAuth, db.session, user=current_user, user_required=False
    )
