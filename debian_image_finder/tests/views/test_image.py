import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestImageView(TestBase):

    def test_image_view(self):
        """
        Test that image page is accessible without login.
        """
        response = self.client.get(
            url_for('.image', uid=self.image.uid)
        )
        self.assertEqual(response.status_code, 200)

    def test_provider_feed_view(self):
        """
        Test that provider page is accessible without login.
        """
        response = self.client.get(
            url_for('.feed', vendor=self.provider.vendor)
        )
        self.assertEqual(response.status_code, 200)

    def test_provider_edit_view(self):
        """
        Test that provider edit page is inaccessible without login
        and redirects to index page.
        """
        response = self.client.get(
            url_for('.edit_provider', vendor=self.provider.vendor)
        )
        redirect_url = url_for('.index')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)


if __name__ == '__main__':
    unittest.main()
