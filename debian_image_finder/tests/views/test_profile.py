import unittest
from flask import url_for
from debian_image_finder.tests.views.base import TestBase


class TestProfileView(TestBase):

    def test_profile_view(self):
        """
        Test that profile page is inaccessible without login
        and redirects to index page.
        """
        response = self.client.get(
            url_for('.profile')
        )
        redirect_url = url_for('.index')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)


if __name__ == '__main__':
    unittest.main()
