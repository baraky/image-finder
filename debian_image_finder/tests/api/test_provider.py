import json
import unittest
from debian_image_finder.tests.api.base import TestBase
from debian_image_finder.models.provider import Provider
from debian_image_finder.extensions.database import db


class TestProviderApi(TestBase):
    """Tests for the Provider API."""

    def test_post_provider(self):
        args = {
            'name': 'provider',
            'vendor': 'vendor',
            'description': 'description',
            'markdown': 'markdown'
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.post(
            '/api/v1/provider',
            data=json.dumps(args),
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 201)
        self.assertIn('provider', data['name'])
        self.assertIn('description', data['description'])
        self.assertIn('vendor', data['vendor'])
        self.assertIn('markdown', data['markdown'])

    def test_get_provider(self):
        """Ensure the /provider route behaves correctly."""

        provider = Provider(
            name='provider',
            vendor='vendor',
            description='test',
            markdown='markdown'
        )
        db.session.add(provider)
        db.session.commit()

        response = self.client.get('/api/v1/provider/vendor')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('provider', data['name'])
        self.assertIn('vendor', data['vendor'])
        self.assertIn('test', data['description'])
        self.assertIn('markdown', data['markdown'])

    def test_put_provider(self):
        """Ensure the /provider route behaves correctly."""

        provider = Provider(
            name='provider',
            vendor='vendor',
            description='description',
            markdown='markdown'
        )
        db.session.add(provider)
        db.session.commit()

        args = {
            'name': 'provider_change',
            'vendor': 'vendor_change',
            'description': 'description_change',
            'markdown': 'markdown_change'
        }

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.put(
            '/api/v1/provider/vendor',
            data=json.dumps(args),
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('provider_change', data['name'])
        self.assertIn('vendor_change', data['vendor'])
        self.assertIn('description_change', data['description'])
        self.assertIn('markdown_change', data['markdown'])

    def test_delete_provider(self):
        """Ensure the /provider route behaves correctly."""

        provider = Provider(
            name='provider',
            vendor='vendor',
            description='description',
            markdown='markdown'
        )
        db.session.add(provider)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }

        response = self.client.delete(
            '/api/v1/provider/vendor',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('Provider deleted successfully.', data['message'])


if __name__ == '__main__':
    unittest.main()
