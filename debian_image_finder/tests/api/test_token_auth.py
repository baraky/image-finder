import json
import unittest
from dynaconf import settings
from debian_image_finder.tests.api.base import TestBase
from debian_image_finder.utils.token import encode_jwt


class TestTokenAuth(TestBase):
    """Tests for token auth decorator."""

    def test_missing_authorization_header_token(self):
        headers = {
            'Content-Type': 'application/json',
        }

        response = self.client.get(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 401)
        self.assertIn(data['error'], 'Authorization header not in request.')

    def test_missing_token(self):
        headers = {
            'Content-Type': 'application/json',
            'Authorization': ''
        }

        response = self.client.get(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 401)
        self.assertIn(data['error'], 'Token is missing.')

    def test_invalid_token(self):
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'invalid'
        }

        response = self.client.get(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 401)
        self.assertIn(data['error'], 'Invalid token.')

    def test_error_decoding_signature(self):
        payload = self.payload
        settings.set('SECRET_KEY', 'another_key')
        token = encode_jwt(payload)

        settings.set('SECRET_KEY', 'my_precious')
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}'
        }

        response = self.client.get(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 401)
        self.assertIn(data['error'], 'Error decoding signature.')

    def test_invalid_token_public_id(self):
        payload = self.payload
        payload['extra_data']['token_public_id'] = 'none'
        token = encode_jwt(payload)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}'
        }

        response = self.client.get(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 404)
        self.assertIn(data['error'], 'Token not found.')

    def test_invalid_user_public_id(self):
        payload = self.payload
        payload['extra_data']['user_public_id'] = 'none'
        token = encode_jwt(payload)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}'
        }

        response = self.client.get(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 401)
        self.assertIn(data['error'], 'User not allowed to perform this action.')

    def test_non_admin_user_public_id(self):
        payload = self.payload
        payload['extra_data']['user_public_id'] = self.user.public_id
        token = encode_jwt(payload)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}'
        }

        response = self.client.get(
            f'/api/v1/user/{self.user.public_id}',
            headers=headers
        )

        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 401)
        self.assertIn(data['error'], 'User not allowed to perform this action.')


if __name__ == '__main__':
    unittest.main()
