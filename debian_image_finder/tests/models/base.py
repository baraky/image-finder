from flask_testing import TestCase
from datetime import datetime, timedelta
from debian_image_finder import create_app
from debian_image_finder.extensions.database import db
from debian_image_finder.models.user import User
from debian_image_finder.models.provider import Provider
from debian_image_finder.models.service_token import ServiceToken
from debian_image_finder.utils.token import generate_payload, encode_jwt


class TestBase(TestCase):

    token = None
    admin_user = None
    provider = None

    def create_app(self):
        app = create_app()
        return app

    def setUp(self):
        """
        Will be called before every test
        """
        db.session.commit()
        db.drop_all()
        db.create_all()
        self.admin_user = User(
            name='admin',
            username='admin',
            email='admin@email.com',
            avatar_url='',
            admin=True
        )

        self.provider = Provider(
            name='AWS',
            vendor='ec2',
            description='',
            markdown=''
        )

        expires_at = datetime.utcnow() + timedelta(minutes=1)
        self.service_token = ServiceToken(
            name='name',
            user_id=self.admin_user.id,
            expires_at=expires_at
        )

        db.session.add(self.admin_user)
        db.session.add(self.provider)
        db.session.add(self.service_token)
        db.session.commit()

        payload = generate_payload(
            'debian-cloud-image-finder',
            minutes=1,
            extra_data={
                'user_public_id': self.admin_user.public_id,
                'token_public_id': self.service_token.public_id
            }
        )
        self.token = encode_jwt(payload)

    def tearDown(self):
        """
        Will be called after every test
        """
        db.session.remove()
        db.drop_all()
