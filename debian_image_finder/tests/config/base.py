from flask_testing import TestCase
from debian_image_finder import create_app
from debian_image_finder.extensions.database import db


class TestBase(TestCase):

    def create_app(self):
        app = create_app()
        return app

    def setUp(self):
        """
        Will be called before every test
        """
        db.session.commit()
        db.drop_all()
        db.create_all()

    def tearDown(self):
        """
        Will be called after every test
        """
        db.session.remove()
        db.drop_all()
