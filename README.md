# Debian Cloud Image Finder

This app aims to make Debian official cloud images more easily accessible to users. Users will be able to search for images based on their attributes, follow instructions to run the images on providers and also follow a RSS feed which will notify about new images releases.

## Dependencies

- docker

## Running the app

- Run app: `make run`
- Seed database: `make seed`
- Upgrade database migrations: `make migrate-db`
- Apply database migrations: `make upgrade-db`
- Drop and recreate database: `make recreate-db`

#### User

- Promote user to admin: `make promote-user`
- Unpromote user: `make unpromote-user`

#### Utilities

- Shell inside container: `make shell`
- Build docker image: `make build`

## Running tests

- Run tests: `make test`
- Run module tests individually: `make test-module module-name=moduleName`
- Run lint: `make lint`
- Run coverage: `make coverage`
- Run coverage report: `make coverage-report`