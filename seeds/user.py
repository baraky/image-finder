import logging
from random import choice
from flask_seeder import Seeder, Faker, generator
from dynaconf import settings
from debian_image_finder.models.user import User
from debian_image_finder.extensions.database import db


class UserSeeder(Seeder):

    def __init__(self):
        self.priority = 1

    def run(self):
        if settings.SKIP_USER_SEEDER:
            return

        email_generator = generator.Email()
        email_generator._domains = ['debian.org']

        faker = Faker(
            cls=User,
            init={
                "name": generator.Name(),
                'username': '',
                'email': email_generator,
                'avatar_url': '',
                "admin": False
            }
        )

        for user in faker.create(20):
            existent_user = User.find_by_username(user.username)
            if existent_user:
                continue
            user.username = user.name.lower()
            user.admin = choice([True, False])
            logging.info(f'Adding user: {user}')
            db.session.add(user)
