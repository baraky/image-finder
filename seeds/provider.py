import json
import logging
from flask_seeder import Seeder
from debian_image_finder.models.provider import Provider
from debian_image_finder.extensions.database import db


class ProviderSeeder(Seeder):

    def __init__(self):
        self.priority = 1

    def run(self):
        json_file = open('seeds/seed.json', 'r')
        data = json.load(json_file)

        for provider in data['providers']:
            logging.info('Name: ' + provider['name'])
            logging.info('Description: ' + provider['description'])
            logging.info('Vendor: ' + provider['vendor'])
            logging.info('Markdown File: ' + provider['markdown'])
            logging.info('')
            vendor = provider['vendor']
            markdown_file = open(f'seeds/markdown/{vendor}.md', 'r')
            markdown_content = markdown_file.read()
            existent_provider = Provider.find_by_vendor(vendor)

            if existent_provider:
                existent_provider.name = provider['name']
                existent_provider.description = provider['description']
                existent_provider.markdown = markdown_content
                db.session.add(existent_provider)
                continue

            db.session.add(
                Provider(
                    name=provider['name'],
                    description=provider['description'],
                    vendor=provider['vendor'],
                    markdown=markdown_content
                )
            )
